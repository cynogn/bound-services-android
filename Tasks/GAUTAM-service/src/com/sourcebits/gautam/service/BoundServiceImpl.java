package com.sourcebits.gautam.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class BoundServiceImpl extends Service{
	private final IBinder mBinder = new IBinderImpl();
	private static final String TAG = BoundServiceImpl.class.getCanonicalName(); 
	
	@Override
	public IBinder onBind(Intent arg0) {
		Log.d(TAG, "onBind");
		return mBinder;
	}
	
	@Override
	public void onRebind(Intent intent) {
		// TODO Auto-generated method stub
		super.onRebind(intent);
	}
	
	public class IBinderImpl extends Binder{
		public BoundServiceImpl getService(){
			Log.d(TAG, "getService()");
			return BoundServiceImpl.this;
		}
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}
	
	public void executeService(){
		Log.d(TAG, "Execute Service");
	}
	@Override
	public boolean onUnbind(Intent intent) {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}

}
