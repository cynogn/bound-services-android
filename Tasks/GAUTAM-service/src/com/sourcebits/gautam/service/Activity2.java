package com.sourcebits.gautam.service;

import com.sourcebits.gautam.service.BoundServiceImpl.IBinderImpl;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;

public class Activity2 extends Activity {

	private BoundServiceImpl mBoundService;
	final ServiceConnection serviceConnection = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {

		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			IBinderImpl binderImpl = (IBinderImpl) service;
			mBoundService = binderImpl.getService();
			mBoundService.executeService();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		// startService();
	}

	private void startService() {
		// startService(new Intent(ServiceActivity.this,
		// StartedServiceImpl.class));
		startService(new Intent(Activity2.this, IntentServiceImpl.class));
		Runnable serviceStartRunnable = new Runnable() {

			public void run() {
				// startService(new Intent(ServiceActivity.this,
				// StartedServiceImpl.class));
				startService(new Intent(Activity2.this, IntentServiceImpl.class));
			}
		};

	}

	public void bindService(View view) {
		Intent bindIntent = new Intent(Activity2.this, BoundServiceImpl.class);
		bindService(bindIntent, serviceConnection, BIND_AUTO_CREATE);
	}

}
